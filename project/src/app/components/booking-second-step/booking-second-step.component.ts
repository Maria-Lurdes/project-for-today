import { Component, OnInit, ViewChild } from '@angular/core';
import { NzRateComponent } from '../../../../node_modules/ng-zorro-antd';
import { element } from '../../../../node_modules/@angular/core/src/render3/instructions';


@Component({
  selector: 'app-booking-second-step',
  templateUrl: './booking-second-step.component.html',
  styleUrls: ['./booking-second-step.component.css'],

})
export class BookingSecondStepComponent implements OnInit {

  constructor() { }


  disable: boolean = true;
  activate: boolean = false;
  selectValue = '';
  selectValue1 = "";
  pay: number = 0.00;
  disableBtn = true;

  pay1: number = 0.00;
  payment: boolean = false;
  choose = [];
  array;
  choosen_rooms: boolean = false;
  template: boolean = true;
  info = [{
    type: "TWN - Standart NzRateComponent",
    price: 100000,
    uzc: "UZC",
    present: 2,
    buttonActive: false,
    resident: "",
    text: "Взрослых(1), Детей (2), Ночей (3)",
    date: "23.06.2019-25.06.2019"
  },
  {
    type: "TWN - Standart NzRateComponent",
    price: 100000,
    uzc: "UZC",
    buttonActive: false,
    present: 5,
    resident: "Resident",
    text: "Взрослых(1), Детей (2), Ночей (3)",
    date: "23.06.2019-25.06.2019"
  },
  {
    type: "TWN - Delux",
    price: 100000,
    uzc: "UZC",
    buttonActive: false,
    present: 3,
    resident: "",
    text: "Взрослых(1), Детей (2), Ночей (3)",
    date: "23.06.2019-25.06.2019"
  },
  {
    type: "TWN - Delux",
    price: 100000,
    uzc: "UZC",
    buttonActive: false,
    present: 0,
    resident: "Resident",
    text: "Взрослых(1), Детей (2), Ночей (3)",
    date: "23.06.2019-25.06.2019"
  }];
  array_rooms = [];

  edit(element) {
    let min = 1000;
    let max = 1000;
    let random = Math.floor(Math.random() * (+max - +min)) + +min;
    element.price += random;
  }

  sum(element) {
    let priceElement;
    priceElement = element.price;
    this.pay += priceElement;
    console.log(21, this.pay);
    this.payment = true;

    let person: object;
    person = element;
    element.buttonActive = false;
    this.choose.push(JSON.parse(JSON.stringify(person)));
  }

  sum1(element) {
    let priceElement1;
    priceElement1 = element.price;
    this.pay1 += priceElement1;
    this.choosen_rooms = true;
    this.template = false;

    let person: object;
    person = element;
    this.array_rooms.push(JSON.parse(JSON.stringify(person)));
    this.disableBtn = false;

  }

  delete() {
    this.pay = 0;
    this.choose = [];
    this.pay1 = 0;
  }

  changeSelect(index) {
    this.info[index].buttonActive = true;
  }
  changeSelect1(y) {
    this.choose[y].buttonActive = true;
  }
  ngOnInit() {
    this.array = this.info;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingSecondStepComponent } from './booking-second-step.component';

describe('BookingSecondStepComponent', () => {
  let component: BookingSecondStepComponent;
  let fixture: ComponentFixture<BookingSecondStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingSecondStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingSecondStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

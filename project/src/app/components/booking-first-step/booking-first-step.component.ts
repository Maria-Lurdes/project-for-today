import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { BookingSecondStepComponent } from '../booking-second-step/booking-second-step.component';
// import { BookingSecondStepComponent } from "../booking-second-step/booking-second-step.component"

@Component({
  selector: 'app-booking-first-step',
  templateUrl: './booking-first-step.component.html',
  styleUrls: ['./booking-first-step.component.css'],
  // providers: [BookingSecondStepComponent]
})
export class BookingFirstStepComponent implements OnInit {

  // @Input () choosen_rooms;
  constructor() { }

  @ViewChild(BookingSecondStepComponent)
  private counterComponent: BookingSecondStepComponent;
  pay;
  first: boolean = true;
  second: boolean = false;
  block: boolean = true;

  showSecond() {
    this.second = true;
    this.first = false;
  }

  svg(array, y) {
    array.array_rooms.splice(y, 1);
    if (array.array_rooms == []) {
      array.disableBtn = true;
    }
  }
  ngOnInit() {
  }
  ngAfterViewInit(): void {
    console.log(42, this.counterComponent);
  }
}

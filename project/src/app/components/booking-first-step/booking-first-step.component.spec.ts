import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingFirstStepComponent } from './booking-first-step.component';

describe('BookingFirstStepComponent', () => {
  let component: BookingFirstStepComponent;
  let fixture: ComponentFixture<BookingFirstStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingFirstStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingFirstStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { BookingFirstStepComponent } from './components/booking-first-step/booking-first-step.component';
import { RouterModule, Routes } from '@angular/router';
import { NzIconModule } from 'ng-zorro-antd';
import { NzSelectModule } from 'ng-zorro-antd';
import { NzButtonModule } from 'ng-zorro-antd';
import { NzDatePickerModule } from 'ng-zorro-antd';
import { TemplateComponent } from './components/template/template.component';
import { TemplateInfoComponent } from './components/template-info/template-info.component';
import { BookingSecondStepComponent } from './components/booking-second-step/booking-second-step.component';
import { NzInputModule } from 'ng-zorro-antd';
import { ChoosenRoomsComponent } from './components/choosen-rooms/choosen-rooms.component';
registerLocaleData(en);

const appRoutes: Routes = [
  { path: '', component: BookingFirstStepComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    BookingFirstStepComponent,
    TemplateComponent,
    TemplateInfoComponent,
    BookingSecondStepComponent,
    ChoosenRoomsComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    NzIconModule,
    NzSelectModule,
    NzButtonModule,
    NzDatePickerModule,
    NzInputModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
